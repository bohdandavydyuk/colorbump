﻿public static class Constance
{
    public static bool PlayMusic { get; set; }

    public static float GetNormalSpeedBall
    {
        get
        {
#if (UNITY_ANDROID || UNITY_IOS || UNITY_IPHONE) && !UNITY_EDITOR
        
              return 0.6f;
#else
            return 0.15f;
#endif
        }
    }


    public static float GetCoefAddSpeedBall_X
    {
        get
        {
#if (UNITY_ANDROID || UNITY_IOS || UNITY_IPHONE) && !UNITY_EDITOR
        
               return 15;
#else
            return 10;
#endif
        }
    }


    public static float GetCoefAddSpeedBall_Y
    {
        get
        {
#if (UNITY_ANDROID || UNITY_IOS || UNITY_IPHONE) && !UNITY_EDITOR
     
               return 35;
#else
            return 18;
#endif
        }
    }

}
