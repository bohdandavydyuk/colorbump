﻿using UnityEngine;

public class AudioManager : MonoBehaviour
{
    [SerializeField]
    private AudioSource _audioSource;

    public static AudioManager Instance;

    [System.Serializable]
    public class Sound
    {
        public AudioClip clip;
    }

    public Sound Background;

    private void Awake()
    {
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        int Music = PlayerPrefs.GetInt("PlayMusic", 1);

        if (Music == 1)
        {
            if (!Constance.PlayMusic)
            {
                Constance.PlayMusic = true;
                PlayBackgroundMusic(true);
            }
        }
    }

    public void PlayBackgroundMusic(bool play)
    {
        if (play)
        {
            _audioSource.clip = Background.clip;
            _audioSource.Play();
        }
        else
        {
            _audioSource.Stop();
        }

    }
}

