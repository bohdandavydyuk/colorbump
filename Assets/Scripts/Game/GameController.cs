﻿using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField]
    private Transform _containerForLevel;

    List<GameObject> AllConstructList = new List<GameObject>();

    public bool GameOver { get; set; }

    void OnEnable()
    {
        CreateNewLevel();
    }

    void CreateNewLevel()
    {
        int[] ArrayWithNumbersConstruct = GetArrayWithNumbersConstruct();

        for (int i = 0; i < ArrayWithNumbersConstruct.Length; i++)
        {
            GameObject go = Instantiate(Resources.Load("Construct_" + ArrayWithNumbersConstruct[i].ToString(), typeof(GameObject)) as GameObject);
            go.transform.parent = _containerForLevel.transform;
            go.transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
            if (AllConstructList.Count == 0)
            {
                go.transform.localPosition = new Vector3(0, 0, 120);
            }
            else
            {
                go.transform.localPosition = new Vector3(0, 0, AllConstructList[AllConstructList.Count - 1].transform.localPosition.z + 120);
            }
            go.SetActive(true);
            AllConstructList.Add(go);

            if (i == ArrayWithNumbersConstruct.Length - 1)
            {
                GameObject Finish = Instantiate(Resources.Load("Finish", typeof(GameObject)) as GameObject);
                Finish.transform.parent = _containerForLevel.transform;
                Finish.transform.localScale = new Vector3(0.77f, 1, 1);
                Finish.transform.localPosition = new Vector3(0, 0, AllConstructList[AllConstructList.Count - 1].transform.localPosition.z + 100);
                Finish.SetActive(true);
            }
        }
    }

    int[] GetArrayWithNumbersConstruct()
    {
        // 1. Саздаем массив с кол-вом элементов =  "номер уровня" + 3. 
        // 2. Забиваем массив числами от 1 до кол-во элементов
        // 3. Перемешиваем элементы массива
        // Таким образом каждый раз генерируется массив с номерами конструкций, причем конструкции не повторяються в уровне.

        int[] SameArray = new int[PlayerPrefs.GetInt("LastPlayerLevel", 1) + 3];

        for (int i = 0; i < SameArray.Length; i++)
        {
            SameArray[i] = i + 1;
        }

        System.Random RandObj = new System.Random();

        for (int i = SameArray.Length - 1; i >= 1; i--)
        {
            int j = RandObj.Next(i + 1);

            int Temp = SameArray[j];
            SameArray[j] = SameArray[i];
            SameArray[i] = Temp;
        }

        return SameArray;
    }

}
