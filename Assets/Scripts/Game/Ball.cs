﻿using UnityEngine;

public class Ball : MonoBehaviour
{
    [SerializeField]
    public GameController _gameController;

    [SerializeField]
    public GameUIManager _gameUIManager;

    public bool StartMove { get; set; }

    float NormalSpeed_X;

    float NormalSpeed_Z;

    void Start()
    {
        NormalSpeed_Z = Constance.GetNormalSpeedBall;
    }

    void Update()
    {
        if (StartMove & !_gameController.GameOver)
        {
            transform.position = gameObject.transform.position + new Vector3(NormalSpeed_X, 0, NormalSpeed_Z);
        }

        if (transform.localPosition.x > 29.9f || transform.localPosition.x < 0.17f)// Выход за левый и правый край
        {
            Camera.main.transform.GetComponent<GameCamera>().StartMove = false;

            gameObject.transform.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;

            gameObject.transform.GetComponent<Rigidbody>().mass = 50;          

            StartMove = false;

            _gameController.GameOver = true;

            _gameUIManager.GameOver();

        }
    }

    public void AddSpeed(Vector3 speed)
    {
        NormalSpeed_X = speed.x*Constance.GetCoefAddSpeedBall_X;

       NormalSpeed_Z = Constance.GetNormalSpeedBall + (speed.y * Constance.GetCoefAddSpeedBall_Y);
    }

    public void NormalizeSpeed()
    {
        NormalSpeed_X = 0;
        NormalSpeed_Z = Constance.GetNormalSpeedBall;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Danger")
        {
            Camera.main.transform.GetComponent<GameCamera>().StartMove = false;

            gameObject.transform.GetComponent<Rigidbody>().isKinematic = true;

            _gameController.GameOver = true;

            StartMove = false;

            _gameUIManager.GameOver();
        }
    }

    private void OnTriggerExit(Collider Collider)
    {
        if (Collider.tag == "Finish")
        {
            _gameUIManager.WinLevel();
        }
    }
}
