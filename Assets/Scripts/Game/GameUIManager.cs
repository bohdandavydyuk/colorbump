﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameUIManager : MonoBehaviour
{
    [SerializeField]
    private GameObject _tapToStart;

    [SerializeField]
    private GameObject _gameOverWindow;

    [SerializeField]
    private GameObject _win;

    [SerializeField]
    private Text _textCurrentLevel;

    Coroutine BlinkTapToStartCoroutine;

    void OnEnable()
    {
        _tapToStart.SetActive(true);
        _textCurrentLevel.text = "Level " + PlayerPrefs.GetInt("LastPlayerLevel", 1).ToString();
        _textCurrentLevel.gameObject.SetActive(true);
        BlinkTapToStartCoroutine = StartCoroutine(BlinkTapToStart());
    }

    IEnumerator BlinkTapToStart()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.5f);
            _tapToStart.SetActive(true);
            yield return new WaitForSeconds(0.5f);
            _tapToStart.SetActive(false);
        }
    }

    public void StartSession()
    {
        if (BlinkTapToStartCoroutine != null)
        {
            StopCoroutine(BlinkTapToStartCoroutine);
        }
        _textCurrentLevel.gameObject.SetActive(false);
        _tapToStart.SetActive(false);
    }

    public void GameOver()
    {
        StartCoroutine(Failed());
    }

    IEnumerator Failed()
    {
        yield return new WaitForSeconds(1.5f);

        _gameOverWindow.SetActive(true);
    }

    public void WinLevel()
    {
        StartCoroutine(Win());
    }

    IEnumerator Win()
    {
        yield return new WaitForSeconds(0.2f);

        _win.SetActive(true);

        int CurrentLevel = PlayerPrefs.GetInt("LastPlayerLevel", 1);

        // Всех конструкций сделал 8, а в каждом уровне кол-во конструкций = "номер уровня" + 3. Поэтому макс. уровень 5. 

        if (CurrentLevel < 5)
        {
            int NewLevel = CurrentLevel + 1;
            PlayerPrefs.SetInt("LastPlayerLevel", NewLevel);   
        }
        else
        {
            PlayerPrefs.SetInt("LastPlayerLevel", 1);
        }

        yield return new WaitForSeconds(2f);

        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }
}
