﻿using UnityEngine;

public class GameCamera : MonoBehaviour
{
    [SerializeField]
    private GameObject _ball;

    [SerializeField]
    public GameController _gameController;

    [SerializeField]
    public GameUIManager _gameUIManager;

    private Vector2 PreviousFingerPosition;

    public bool StartMove { get; set; }

    private Vector2 GetFingerWorldPosition
    {
        get
        {
#if (UNITY_ANDROID || UNITY_IOS || UNITY_IPHONE) && !UNITY_EDITOR
			            if (Input.touchCount == 1 )
				            return Camera.main.ScreenToViewportPoint(Input.GetTouch(0).position);
			            else
				            return Camera.main.ScreenToViewportPoint(Vector2.zero);

#else
            return Camera.main.ScreenToViewportPoint(Input.mousePosition);
#endif
        }
    }

    void Update()
    {
        if (StartMove)
        {
            transform.position = gameObject.transform.position + new Vector3(0, 0, Constance.GetNormalSpeedBall);
        }

#if (UNITY_ANDROID || UNITY_IOS || UNITY_IPHONE) && !UNITY_EDITOR
        
        if (Input.touchCount != 2)
        {
            if (!_gameController.GameOver)
            {
                 HandleTouchInput();
            }  
        }
#else
        if (!_gameController.GameOver)
        {
            HandleMouseInput();
        }
#endif
    }

    private void HandleTouchInput()
    {
        if (Input.touchCount == 1)
        {
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began)
            {
                if (!_ball.transform.GetComponent<Ball>().StartMove)
                {
                    _ball.transform.GetComponent<Ball>().StartMove = true;

                    _gameUIManager.StartSession();

                    StartMove = true;
                }

                PreviousFingerPosition = GetFingerWorldPosition;
            }
            else
            {
                if (touch.phase == TouchPhase.Moved)
                {
                    CalculateSpeedBall();
                }
                else
                {
                    if (touch.phase == TouchPhase.Ended)
                    {
                        _ball.transform.GetComponent<Ball>().NormalizeSpeed();
                    }
                }
            }
        }
    }

    private void HandleMouseInput()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (!_ball.transform.GetComponent<Ball>().StartMove)
            {
                _ball.transform.GetComponent<Ball>().StartMove = true;
                _gameUIManager.StartSession();
                StartMove = true;
            }

            PreviousFingerPosition = GetFingerWorldPosition;
        }
        else
        {
            if (Input.GetMouseButton(0))
            {
                CalculateSpeedBall();
            }
            else
            {
                if (Input.GetMouseButtonUp(0))
                {
                    _ball.transform.GetComponent<Ball>().NormalizeSpeed();
                }
            }
        }
    }


    private void CalculateSpeedBall()
    {
        Vector3 deltaPosition = new Vector3(0, 0, 0);

        deltaPosition = GetFingerWorldPosition - PreviousFingerPosition;

        PreviousFingerPosition = GetFingerWorldPosition;

        _ball.transform.GetComponent<Ball>().AddSpeed(deltaPosition);
    }

}
