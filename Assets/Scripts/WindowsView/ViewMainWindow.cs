﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ViewMainWindow : MonoBehaviour
{
    [SerializeField]
    private Button _buttonPlay;

    [SerializeField]
    private Button _buttonOpenSettings;

    [SerializeField]
    private GameObject _settingsPanel;

    void Start()
    {
        _buttonPlay.onClick.AddListener(Play);
        _buttonOpenSettings.onClick.AddListener(OpenSettings);
    }

    void Play()
    {
        SceneManager.LoadScene("Game");
    }

    void OpenSettings()
    {
        _settingsPanel.SetActive(true);
    }
}
