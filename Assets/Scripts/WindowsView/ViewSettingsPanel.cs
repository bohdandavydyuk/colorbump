﻿using UnityEngine;
using UnityEngine.UI;

public class ViewSettingsPanel : MonoBehaviour
{
    [SerializeField]
    private Button _buttonBack;

    [SerializeField]
    private Button _buttonMusicOn;

    [SerializeField]
    private Button _buttonMusicOff;

    [SerializeField]
    private Sprite _spriteMusicOnActive;

    [SerializeField]
    private Sprite _spriteMusicOnNoActive;

    [SerializeField]
    private Sprite _spriteMusicOffActive;

    [SerializeField]
    private Sprite _spriteMusicOffNoActive;

    private int GetStateBacgroundMusic
    {
        get
        {
            return PlayerPrefs.GetInt("PlayMusic", 1);
        }

        set
        {
            PlayerPrefs.SetInt("PlayMusic", value);
        }
    }

    void Start()
    {
        _buttonMusicOn.onClick.AddListener(MusicOn);
        _buttonMusicOff.onClick.AddListener(MusicOff);
        _buttonBack.onClick.AddListener(BackToMainWindow);

        CheckStateMusicButtons();
    }

    void CheckStateMusicButtons()
    {
        if (GetStateBacgroundMusic == 1)
        {
            _buttonMusicOn.transform.GetComponent<Image>().sprite = _spriteMusicOnActive;
            _buttonMusicOff.GetComponent<Image>().sprite = _spriteMusicOffNoActive;
        }
        else
        {
            _buttonMusicOn.transform.GetComponent<Image>().sprite = _spriteMusicOnNoActive;
            _buttonMusicOff.GetComponent<Image>().sprite = _spriteMusicOffActive;
        }
    }

    void MusicOn()
    {
        if (GetStateBacgroundMusic == 0)
        {
            GetStateBacgroundMusic = 1;
            _buttonMusicOn.transform.GetComponent<Image>().sprite = _spriteMusicOnActive;
            _buttonMusicOff.GetComponent<Image>().sprite = _spriteMusicOffNoActive;
            AudioManager.Instance.PlayBackgroundMusic(true);
        }
    }

    void MusicOff()
    {
        if (GetStateBacgroundMusic == 1)
        {
            GetStateBacgroundMusic = 0;
            _buttonMusicOn.transform.GetComponent<Image>().sprite = _spriteMusicOnNoActive;
            _buttonMusicOff.GetComponent<Image>().sprite = _spriteMusicOffActive;
            AudioManager.Instance.PlayBackgroundMusic(false);
        }
    }

    void BackToMainWindow()
    {
        gameObject.SetActive(false);
    }

}
