﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ViewGameOverWindow : MonoBehaviour
{
    [SerializeField]
    private Button _buttonReplay;

    [SerializeField]
    private Button _buttonBackToMenu;

    void Start()
    {
        _buttonReplay.onClick.AddListener(Replay);
        _buttonBackToMenu.onClick.AddListener(BackToMenu);
    }

    void Replay()
    {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }
    void BackToMenu()
    {
        SceneManager.LoadScene("Menu");
    }
}
